import { bool, collect, list, Parsed, text } from '@gerard2p/mce';
import { cliPath, targetPath } from '@gerard2p/mce/paths';
import { add, remove } from 'os-service';
export let description = 'A description for your command';
export let args = '<name> <script> [program_args...]';
export let options = {
    install: bool('-i', 'Installs a new service'),
    uninstall: bool('-u', 'Uninstalls a service'),
    serviceName: text('', 'The services display name, defaults to the name parameter. (windows only)'),
    nodePath: text('', 'The fully qualified path to the node binary used to run the service (i.e. c:\\Program Files\\nodejs\\node.exe), defaults to the value of process.execPath'),
    nodeArgs: collect('', 'Comma separed values. An array of strings specifying parameters to pass to nodePath'),
    // programPath: text('', 'The program to run using nodePath, defaults to the value of process.argv[1]'),
    // programArgs: collect('', 'An array of strings specifying parameters to pass to programPath'),
    runLevels: list('', 'An array of numbers specifying Linux run-levels at which the service should be started for Linux platforms, defaults to [2, 3, 4, 5], this is only used when chkconfig or update-rc.d is used to install a service'),
    username: text('', 'For Windows platforms a username and password can be specified, the service will be run using these credentials when started, see the CreateService() functions win32 API documentation for details on the format of the username, on all other platforms this parameter is ignored'),
    password: text('', 'See the username parameter'),
    systemdWantedBy: text('', 'For when systemd will be used a target can be specified for the WantedBy attribute under the [Install] section in the generated systemd unit file', 'multi-user.target'),
    dependencies: list('', 'AN array of strings specifying other services this service depends on, this is optional')
};
export async function action(service:string, script:string, programArgs:string[], opt:Parsed<typeof options>) {
    const {install, uninstall} = opt;
    if(install) {
        let OPTIONS = { 
            programPath: cliPath('run'),
            programArgs: programArgs.concat([targetPath(script)])
         };
        for(const key of Object.keys(opt)) {

            if(opt[key]) {
                OPTIONS[key] = opt[key];
            }
        }
        console.log(opt);
        console.log(service, OPTIONS);
        add(service, OPTIONS, function(error) {
            if (error)
                console.trace(error);
        });
    } else if (uninstall) {
        remove (service, function(error) {
            if (error)
                console.trace(error);
        });
    } else {
        console.log('No operation selection [-i, -u]')
    }

}